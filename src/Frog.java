import java.awt.event.KeyEvent;

public class Frog extends Sprite {
    
    private static final int MAX_SPEED_X = 3;
    private static final int MAX_SPEED_Y = 3;
  
    private int speed_x;
    private int speed_y;
    
    public Lick lick = new Lick((Game.getWidth() / 2), (Game.getHeight() / 2));

    public Frog(int x, int y) {
        super(x, y);
        
        initFrog();
    }

    private void initFrog() {
        
        showFrog();        
    }
    
    private void showFrog(){
        loadImage("images/frog.png");
    }
    
    public void move() {
        
        // Limits the movement of the frog to the side edges.
        if((speed_x < 0 && x <= 0) || (speed_x > 0 && x + width >= Game.getWidth())){
            speed_x = 0;
        }
        
        // Moves the frog on the horizontal axis
        x += speed_x;
        
        // Limits the movement of the frog to the vertical edges.
        if((speed_y < 0 && y <= (Game.getHeight()/2)-10) || (speed_y > 0 && y + height >= Game.getHeight())){
            speed_y = 0;
        }

        // Moves the frog on the vertical axis
        y += speed_y;        
    }

    public void keyPressed(KeyEvent e) {

        int key = e.getKeyCode();
        
        // Set speed to move to the left
        if (key == KeyEvent.VK_LEFT) { 
            speed_x = -1 * MAX_SPEED_X;
        }

        // Set speed to move to the right
        if (key == KeyEvent.VK_RIGHT) {
            speed_x = MAX_SPEED_X;
        }
        // Set speed to move to up and set thrust effect
        if (key == KeyEvent.VK_UP) {
            speed_y = -1 * MAX_SPEED_Y;
        }
        
        // Set speed to move to down
        if (key == KeyEvent.VK_DOWN) {
            speed_y = MAX_SPEED_Y;
        }
        
        // Shoot lick
        if (key == KeyEvent.VK_SPACE){
        	lick.shoot(lick);
        }
    }
    
    public void keyReleased(KeyEvent e) {

        int key = e.getKeyCode();

        if (key == KeyEvent.VK_LEFT || key == KeyEvent.VK_RIGHT) {
            speed_x = 0;
        }

        if (key == KeyEvent.VK_UP || key == KeyEvent.VK_DOWN) {
            speed_y = 0;
        }
    }
}