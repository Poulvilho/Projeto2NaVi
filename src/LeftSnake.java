public class LeftSnake extends Sprite {
	
	public int LEVEL_LEFTSNAKE = 2;

    public LeftSnake(int x, int y) {
        super(x, y);
        
        initLeftSnake();
    }

    private void initLeftSnake() {
        
        showLeftSnake();        
    }
    
    private void showLeftSnake(){
        loadImage("images/leftsnake.png"); 
    }    

    public void move() {
        
        // Moves the snake on the verical axis
        x += LEVEL_LEFTSNAKE;
        
        //limit
        if(x >= Game.getWidth()+10){
        	x = rand_X(-100);
        	y = rand_Y(Game.getHeight()/2)-10;        	
        }
    }
    
    private int rand_X(int i) {
    	int num = (int) (Math.random() * 100) + i;
 		return num;
 	}
    
    private int rand_Y(int i) {
    	int num = (int) (Math.random() * (Game.getHeight()/2)-10) + (Game.getHeight()/2)-10;
 		return num;
 	}
}