public class RightSnake extends Sprite {
	
	public int LEVEL_RIGHTSNAKE = 3;

    public RightSnake(int x, int y) {
        super(x, y);
        
        initRightSnake();
    }

    private void initRightSnake() {
        
        showRightSnake();        
    }
    
    private void showRightSnake(){
        loadImage("images/rightsnake.png"); 
    }    

    public void move() {
        
        // Moves the frog on the verical axis
        x -= LEVEL_RIGHTSNAKE;
        //limits
        if(x <= -50){
        	x = rand_X(Game.getWidth()+10);
        	y = rand_Y(Game.getHeight()/2)-10;        	
        }
    }
    
    private int rand_X(int i) {
    	int num = (int) (Math.random() * 100) + i;
 		return num;
 	}
    
    private int rand_Y(int i) {
    	int num = (int) (Math.random() * (Game.getHeight()/2)-10) + (Game.getHeight()/2)-10;
 		return num;
 	}
}