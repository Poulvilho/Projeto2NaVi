import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class InterfaceInicial {

   private JFrame mainFrame;
   private JLabel headerLabel;
   private JLabel creditsLabel;
   private JPanel controlPanel;
  
   public InterfaceInicial(){
      makeGUI();
   }

   public static void main(String[] args){
	   InterfaceInicial game = new InterfaceInicial();  
	   game.showGame();       
   }
      
   private void makeGUI(){
	   
      mainFrame = new JFrame("Radikal Frog");
      mainFrame.setSize(900,590);
      mainFrame.setLayout(new GridLayout(3, 1));

      headerLabel = new JLabel("",JLabel.CENTER );
      creditsLabel = new JLabel("",JLabel.CENTER);
      
      creditsLabel.setSize(350,100);
      
      mainFrame.addWindowListener(new WindowAdapter() {
         public void windowClosing(WindowEvent windowEvent){
	        System.exit(0);
         }        
      });    
      controlPanel = new JPanel();
      controlPanel.setLayout(new FlowLayout());

      mainFrame.add(headerLabel);
      mainFrame.add(controlPanel);
      mainFrame.add(creditsLabel);
      mainFrame.setVisible(true);  
   }

   private void showGame(){
      headerLabel.setText("Radikal Frog"); 
      creditsLabel.setText("Visual Developer: Lucas Porta");
      
      JButton playButton = new JButton("Play");

      playButton.setActionCommand("Play");

      playButton.addActionListener(new ButtonClickListener()); 

      controlPanel.add(playButton);      

      mainFrame.setVisible(true); 
   }

   	private class ButtonClickListener implements ActionListener{
	   	public void actionPerformed(ActionEvent e) {
		String command = e.getActionCommand();  
       	if( command.equals( "Play" ))  {
        	 
    	EventQueue.invokeLater(new Runnable() {
    	@Override
       	public void run() {
    	   	Application app = new Application();
    	   	app.setVisible(true);
    	   	mainFrame.setVisible(false); 
       		}
       	   });
       }  	
	   }		
   }
}