public class Gnat extends Sprite {
	
	public int LEVEL_GNAT = 1;

    public Gnat(int x, int y) {
        super(x, y);
        
        initGnat();
    }

    private void initGnat() {
        
        showGnat();        
    }
    
    private void showGnat(){
        loadImage("images/gnat.png"); 
    }    

    public void move() {
        
        // Moves the frog on the verical axis
        x += LEVEL_GNAT;
        //limits
        if(x >= Game.getWidth()+10){
        	x = rand_X(-100);
        	y = rand_Y();
        	this.setVisible(true);
        }
    }
  
    private int rand_X(int i) {
    	int num = (int) (Math.random() * 100) + i;
 		return num;
 	}
    
    private int rand_Y() {
    	int num = (int) (Math.random() * (Game.getHeight()/2)-20);
 		return num;
 	}
}