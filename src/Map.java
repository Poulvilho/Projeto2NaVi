import java.awt.Color;
import java.awt.Graphics;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Map extends JPanel implements ActionListener {

	private static final long serialVersionUID = 1L;
	private final int FROG_X = 420;
    	private final int FROG_Y = 430;
	private final int LEFTSNAKE_X = -50;
	private final int LEFTSNAKE_Y = (Game.getHeight()/2)-10;
	private final int RIGHTSNAKE_X = Game.getWidth()+10;
	private final int RIGHTSNAKE_Y = Game.getHeight()-50;
	private final int GNAT_X = -50;
	private final int GNAT_Y = (Game.getHeight()/2) - 100;
	private final Timer timer_map;
	private int points;
	private boolean StartGame;

	private final Image background;
	private Frog frog;
	private LeftSnake leftSnake;
	private RightSnake rightSnake;
	private Gnat gnat;
    
	public Map() {
        
        addKeyListener(new KeyListerner());
        
        setFocusable(true);
        setDoubleBuffered(true);
        ImageIcon image = new ImageIcon("images/lake.png");
        
        this.background = image.getImage();

        frog = new Frog(FROG_X, FROG_Y);
        leftSnake = new LeftSnake(LEFTSNAKE_X, LEFTSNAKE_Y);
        rightSnake = new RightSnake(RIGHTSNAKE_X, RIGHTSNAKE_Y);
        gnat = new Gnat (GNAT_X, GNAT_Y);
        points = 0;
        StartGame = false;
        
        
        timer_map = new Timer(Game.getDelay(), this);
        timer_map.start();                            
	}

	@Override
	public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        g.drawImage(this.background, 0, 0, null);
        drawFrog(g);
        drawLeftSnake(g);
        drawRightSnake(g);
        if(StartGame == false){
        	StartMensage(g);
        }
        if(gnat.isVisible() == true){
        	drawGnat(g);
        }
        if(frog.lick.isVisible() == true){
        	drawLick(g);
        	getGnat();
        }
        death(g);
        
        Toolkit.getDefaultToolkit().sync();
	}

	private void drawFrog(Graphics g) {

	// Draw frog
	g.drawImage(frog.getImage(), frog.getX(), frog.getY(), this);
	}
    
    private void drawLeftSnake(Graphics g) {
        
        // Draw LeftSnake
        g.drawImage(leftSnake.getImage(), leftSnake.getX(), leftSnake.getY(), this);
    }
    
    private void drawRightSnake(Graphics g) {
        
        // Draw RightSnake
        g.drawImage(rightSnake.getImage(), rightSnake.getX(), rightSnake.getY(), this);
    }
    
    private void drawGnat(Graphics g) {
        
        // Draw Gnat
        g.drawImage(gnat.getImage(), gnat.getX(), gnat.getY(), this);
    }
    
    private void drawLick(Graphics g) {
        
        // Draw Lick
        g.drawImage(frog.lick.getImage(), frog.getX() + 25, frog.getY() - 380, this);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
    	if(!StartGame){
    	}
    	else if (frog.isVisible() == true){
	    		updateFrog();
	    		updateLeftSnake();
	    		updateRightSnake();
	    		updateGnat();
	    		repaint();
	    	}
    }
    
    private void updateFrog() {
    	frog.move();
    }
  
    private void updateLeftSnake() {
        leftSnake.move();
    }
    
    private void updateRightSnake() {
        rightSnake.move();
    }
    
    private void updateGnat() {
        gnat.move();
    }
    
    private class KeyListerner extends KeyAdapter {
    	
        @Override
        public void keyPressed(KeyEvent e) {
        	if(e.getKeyCode() == KeyEvent.VK_ENTER){
        		frog = new Frog(FROG_X, FROG_Y);
        		leftSnake = new LeftSnake(LEFTSNAKE_X, LEFTSNAKE_Y);
                rightSnake = new RightSnake(RIGHTSNAKE_X, RIGHTSNAKE_Y);
                gnat = new Gnat (GNAT_X, GNAT_Y);
                points = 0;
                if(!StartGame){
            		StartGame = true;
                }
        	}
            frog.keyPressed(e);
        }

        @Override
        public void keyReleased(KeyEvent e) {
            frog.keyReleased(e);
        }
    }
    
    private void death(Graphics g){
    	if (frog.intersect(frog.getBounds(), leftSnake.getBounds(), 15, 15) == true || frog.intersect(frog.getBounds(), rightSnake.getBounds(), 15, 15) == true){
    			
    	    String message = "Game Over";
    	    String messagePont = "Points: " + points;
    	    String messageContinue = "Press ENTER to rematch";
    	    Font font = new Font("Helvetica", Font.BOLD, 40);
    	    Font littlefont = new Font("Helvetica", Font.BOLD, 20);
    	    FontMetrics metric = getFontMetrics(font);
    	    FontMetrics littlemetric = getFontMetrics(littlefont);
    	    
    	    g.setColor(Color.blue);
    	    g.setFont(font);
    	    g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 2, Game.getHeight() / 2 + 30);
    	    g.drawString(messagePont, (Game.getWidth() - metric.stringWidth(messagePont)) / 2, Game.getHeight() / 2 + 100);
    	    
    	    g.setFont(littlefont);
    	    g.drawString(messageContinue, (Game.getWidth() - littlemetric.stringWidth(messageContinue)) / 2, Game.getHeight() / 2 + 150);
    	    
    	    frog.setVisible(false);
    	}
    }
    
    private void StartMensage (Graphics g){
    	String tips = "Use the ARROWS to move and SPACEBAR to take the gnats";
    	String ready = "Press ENTER to start";
    	Font font = new Font("Helvetica", Font.BOLD, 20);
    	FontMetrics metric = getFontMetrics(font);
    	
    	g.setColor(Color.blue);
    	g.setFont(font);
    	g.drawString(tips, (Game.getWidth() - metric.stringWidth(tips)) / 2, Game.getHeight() / 2 + 30);
    	g.drawString(ready, (Game.getWidth() - metric.stringWidth(ready)) / 2, Game.getHeight() / 2 + 100);
    }
    
    private void getGnat(){
    	if (frog.lick.intersect(frog.lick.getBound(frog), gnat.getBounds(), -10, -300) == true){
    		if(gnat.LEVEL_GNAT<5){
    			gnat.LEVEL_GNAT++;
    		} else if (leftSnake.LEVEL_LEFTSNAKE<5){
    			leftSnake.LEVEL_LEFTSNAKE++;
    			rightSnake.LEVEL_RIGHTSNAKE++;
    		}
    		gnat.x = Game.getWidth()+11;
    		points++;
    	}
    	frog.lick.setVisible(false);
    }
}
