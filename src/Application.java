import javax.swing.JFrame;

public class Application extends JFrame {

	private static final long serialVersionUID = 1L;

	public Application() {

        add(new Map());

        setSize(Game.getWidth(), Game.getHeight());

        setTitle("Radikal Frog");
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
    }    
}