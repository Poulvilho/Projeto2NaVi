public final class Game {
    
    private static final int WIDTH = 900;
    private static final int HEIGHT = 590;
    private static final int DELAY = 10;
    
    public static int getWidth(){
        return WIDTH;
    }
    
    public static int getHeight(){
        return HEIGHT;
    }
    
    public static int getDelay(){
        return DELAY;
    }
}