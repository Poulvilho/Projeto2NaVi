import java.awt.Rectangle;

public class Lick extends Sprite {
	
	public int width = 40;
	
	public Lick(int x, int y) {
        super(x, y);
        this.setVisible(false);
        initLick();
    }
	
	private void initLick() {
        
        showLick();        
    }
	
	private void showLick(){
        loadImage("images/lick.png");
    }
	
	public Rectangle getBound(Frog frog){
		return new Rectangle (frog.getX() +20, frog.getY() + 380, frog.getX()+40, frog.getY());
	}
	
	public void shoot(Lick lick){
		lick.setVisible(true);
	}
}